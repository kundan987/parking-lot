package memory;

import entity.ParkingSpot;
import entity.Vehicle;
import enums.SpotType;

import java.util.Map;

public class ParkingFloor {
    private int floorNumber;
    private Map<Integer, ParkingSpot> twowheelerSpotMap;
    private Map<Integer, ParkingSpot> carSpotMap;

    public ParkingFloor(int floorNumber, Map<Integer, ParkingSpot> twowheelerSpotMap, Map<Integer, ParkingSpot> carSpotMap) {
        this.floorNumber = floorNumber;
        this.twowheelerSpotMap = twowheelerSpotMap;
        this.carSpotMap = carSpotMap;
    }

    public void parkVehicle(Vehicle vehicle, ParkingSpot parkingSpot) throws Exception{
        parkingSpot.parkVehicleToSpot(vehicle);
        if(SpotType.TWO_WHEELER_SPOT == parkingSpot.getSpotType()){
            if(twowheelerSpotMap.get(parkingSpot.getNumber()).isFree()){
                twowheelerSpotMap.get(parkingSpot.getNumber()).parkVehicleToSpot(vehicle);
            } else
                throw new Exception("Spot is not free");

        } else {
            if(carSpotMap.get(parkingSpot.getNumber()).isFree()){
                carSpotMap.get(parkingSpot.getNumber()).parkVehicleToSpot(vehicle);
            } else
                throw new Exception("Spot is not free");
        }
    }

}
