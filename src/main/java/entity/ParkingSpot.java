package entity;

import enums.SpotType;

public class ParkingSpot {

    private int number;
    private boolean free;
    private SpotType spotType;
    private Vehicle vehicle;

    public ParkingSpot(int number, SpotType spotType) {
        this.number = number;
        this.free = true;
        this.spotType = spotType;
    }

    public void parkVehicleToSpot(Vehicle vehicle){
        this.vehicle = vehicle;
        free = false;
    }

    public void removeVehicleToSpot(){
        this.vehicle = null;
        free = true;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public boolean isFree() {
        return free;
    }

    public void setFree(boolean free) {
        this.free = free;
    }

    public SpotType getSpotType() {
        return spotType;
    }

    public void setSpotType(SpotType spotType) {
        this.spotType = spotType;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }
}
