package entity;

import java.util.Date;

public class ParkingTicket {

    private int ticketNumber;
    private Date entryTime;
    private int duration;
    private int amountPaid;
    private ParkingRate rate;

    public int getTicketNumber() {
        return ticketNumber;
    }

    public void setTicketNumber(int ticketNumber) {
        this.ticketNumber = ticketNumber;
    }

    public Date getEntryTime() {
        return entryTime;
    }

    public void setEntryTime(Date entryTime) {
        this.entryTime = entryTime;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getAmountPaid() {
        return amountPaid;
    }

    public void setAmountPaid(int amountPaid) {
        this.amountPaid = amountPaid;
    }

    public ParkingRate getRate() {
        return rate;
    }

    public void setRate(ParkingRate rate) {
        this.rate = rate;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ParkingTicket{");
        sb.append("ticketNumber=").append(ticketNumber);
        sb.append(", entryTime=").append(entryTime);
        sb.append(", duration=").append(duration);
        sb.append(", amountPaid=").append(amountPaid);
        sb.append(", rate=").append(rate);
        sb.append('}');
        return sb.toString();
    }
}
