package entity;

import enums.VehicleType;

public class Vehicle {
    private int number;
    private VehicleType vehicleType;
    private ParkingTicket parkingTicket;

    public Vehicle(int number, VehicleType vehicleType) {
        this.number = number;
        this.vehicleType = vehicleType;
    }

    public void assignTicket(ParkingTicket parkingTicket){
        this.parkingTicket = parkingTicket;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public VehicleType getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(VehicleType vehicleType) {
        this.vehicleType = vehicleType;
    }

    public ParkingTicket getParkingTicket() {
        return parkingTicket;
    }

    public void setParkingTicket(ParkingTicket parkingTicket) {
        this.parkingTicket = parkingTicket;
    }
}
