package entity;

import enums.SpotType;
import enums.VehicleType;

public class ParkingRate {

    private VehicleType vehicleType;
    private int rate;

    public ParkingRate(VehicleType vehicleType, int rate) {
        this.vehicleType = vehicleType;
        this.rate = rate;
    }

    public VehicleType getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(VehicleType vehicleType) {
        this.vehicleType = vehicleType;
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }
}
