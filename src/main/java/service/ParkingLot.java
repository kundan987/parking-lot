package service;

import entity.ParkingRate;
import entity.ParkingSpot;
import entity.ParkingTicket;
import entity.Vehicle;
import enums.SpotType;
import enums.VehicleType;
import memory.ParkingFloor;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class ParkingLot {

    private String name;
    private int twowheelerSpotCount;
    private int maxTwowheelerSpotCount;
    private int carSpotCount;
    private int maxCarSpotCount;
    private int counter;
    private Map<Integer, ParkingFloor> parkingFloorMap;
    private Map<Integer, ParkingTicket> tickets;

    private static ParkingLot instance;

    private ParkingLot(){
        //data initialize here
        name = "Parking Lot";
        counter =1;
        twowheelerSpotCount =0;
        carSpotCount =0;
        maxTwowheelerSpotCount = 2;
        maxCarSpotCount = 2;
        parkingFloorMap = new HashMap<>();

        Map<Integer, ParkingSpot> twowheelerSpotMap = new HashMap<>();
        twowheelerSpotMap.put(0, new ParkingSpot(0, SpotType.TWO_WHEELER_SPOT));
        twowheelerSpotMap.put(1, new ParkingSpot(1, SpotType.TWO_WHEELER_SPOT));

        Map<Integer, ParkingSpot> carSpotMap = new HashMap<>();
        carSpotMap.put(0, new ParkingSpot(0, SpotType.CAR_SPOT));
        carSpotMap.put(1, new ParkingSpot(1, SpotType.CAR_SPOT));

        ParkingFloor parkingFloor = new ParkingFloor(1, twowheelerSpotMap, carSpotMap);

        parkingFloorMap.put(1, parkingFloor);
        tickets = new HashMap<>();
    }

    public static ParkingLot getInstance(){
        if(instance==null)
            instance = new ParkingLot();
        return instance;
    }

    public ParkingTicket getTicekt(Vehicle vehicle) throws Exception {
        if(!isParkingFull(vehicle.getVehicleType())){
            ParkingTicket parkingTicket = new ParkingTicket();
            parkingTicket.setTicketNumber(counter++);
            parkingTicket.setEntryTime(new Date());

            vehicle.assignTicket(parkingTicket);

            for(Map.Entry<Integer,ParkingFloor> map : parkingFloorMap.entrySet()){
                ParkingFloor parkingFloor = map.getValue();
                ParkingSpot parkingSpot = null;
                if(VehicleType.CAR == vehicle.getVehicleType()){

                    parkingTicket.setRate(new ParkingRate(vehicle.getVehicleType(), 20));
                    parkingSpot = new ParkingSpot(carSpotCount, SpotType.CAR_SPOT);
                    carSpotCount++;
                } else {

                    parkingTicket.setRate(new ParkingRate(vehicle.getVehicleType(), 10));
                    parkingSpot = new ParkingSpot(twowheelerSpotCount, SpotType.TWO_WHEELER_SPOT);
                    twowheelerSpotCount++;
                }
                parkingFloor.parkVehicle(vehicle, parkingSpot);
            }
            tickets.put(parkingTicket.getTicketNumber(), parkingTicket);
            return parkingTicket;
        } else
            throw new Exception("Parking is full");

    }

    public ParkingTicket exitParking(Vehicle vehicle, ParkingTicket parkingTicket){
        Date date = new Date();
        int hours = date.compareTo(parkingTicket.getEntryTime());
        parkingTicket.setDuration(hours);
        parkingTicket.setAmountPaid(hours*parkingTicket.getRate().getRate());
        for(Map.Entry<Integer,ParkingFloor> map : parkingFloorMap.entrySet()){
            ParkingFloor parkingFloor = map.getValue();
            ParkingSpot parkingSpot = null;
            if(VehicleType.CAR == vehicle.getVehicleType()){

                parkingTicket.setRate(new ParkingRate(vehicle.getVehicleType(), 20));
                parkingSpot = new ParkingSpot(carSpotCount, SpotType.CAR_SPOT);
                carSpotCount--;
            } else {

                parkingTicket.setRate(new ParkingRate(vehicle.getVehicleType(), 10));
                parkingSpot = new ParkingSpot(twowheelerSpotCount, SpotType.TWO_WHEELER_SPOT);
                twowheelerSpotCount--;
            }
            //parkingFloor.parkVehicle(vehicle, parkingSpot);
        }
        return parkingTicket;
    }

    public boolean isParkingFull(VehicleType vehicleType){

        if(VehicleType.CAR == vehicleType){
            return carSpotCount>=maxCarSpotCount;
        } else {
            return twowheelerSpotCount>= maxTwowheelerSpotCount;
        }
    }

    public boolean isParkingFull(){

            return carSpotCount>=maxCarSpotCount || twowheelerSpotCount>= maxTwowheelerSpotCount;
    }



}
