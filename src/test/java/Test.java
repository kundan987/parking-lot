import entity.ParkingTicket;
import entity.Vehicle;
import enums.VehicleType;
import service.ParkingLot;

public class Test {

    public static void main(String[] args) throws Exception {
        ParkingLot parkingLot = ParkingLot.getInstance();
        System.out.println(parkingLot.isParkingFull());

        Vehicle vehicle1 = new Vehicle(123, VehicleType.CAR);
        ParkingTicket parkingTicket1 = parkingLot.getTicekt(vehicle1);
        System.out.println(parkingTicket1);

        Vehicle vehicle2 = new Vehicle(165, VehicleType.CAR);
        ParkingTicket parkingTicket2 = parkingLot.getTicekt(vehicle2);
        System.out.println(parkingTicket2);

        try{
            Vehicle vehicle3 = new Vehicle(165, VehicleType.CAR);
            ParkingTicket parkingTicket3 = parkingLot.getTicekt(vehicle3);
            System.out.println(parkingTicket3);
        } catch (Exception e){
            System.out.println(e.getMessage());
        }


        parkingTicket1 = parkingLot.exitParking(vehicle1, parkingTicket1);
        System.out.println(parkingTicket1);

        /*parkingTicket1 = parkingLot.getTicekt(vehicle1);
        System.out.println(parkingTicket1);*/

        System.out.println(parkingLot.isParkingFull());
    }
}
